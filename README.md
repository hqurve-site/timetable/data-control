# Auto-downloading

This script can be run by CI or manually. It downloads the data from
uwi's website, checks if there are changes, and commits and pushes changes (if present).

```
npm run _ci-download
```

It uses the `out/_ci-download` directory temporarily (and removes it after use).

# Updating config
The configurations for the extractor are stored in `config`. After adjusting, delete the
timetables which need to be fixed and run `npm run parse-missing [path]`.

# Parsing

The path must be specified. Usually, I use something like `out/temp`. The directory must have the following layout
```
- download: contains the download history repo (possibly a symlink)
- extractor: binary for the extractor
```
On windows, symlinks are weird. So, just copy the executable.

All important files are stored in the `data` directory

Commands: 

- Gathering commit list:
  
  `npm run gather-catalog-commits [path]`


- Parsing a single commit:

  `npm run parse-single-commit [path] [hash]`


- Parsing all missing commits which were not parsed before:
  
  `npm run parse-missing [path]`

  Missing commits are detected by a missing `data/timetables/[hash]` file

- Parsing all parses which had unknown parses:
  
  `npm run parse-unknown [path]`

  

# Site

- `gen-site`:
    `npm run gen-site [path]`

    This command transforms the output of the parsed timetables and produces the format desired of the site.

# Misc commands

- `config`: 
    `npm run gen-config [stage|all] [path]`

    This command produces the configuration files for the `extractor`.
- `inference`: 
    `npm run inference [stage|all] [path]`

    This command takes the output from the extractor and puts it in a more human-readable format.
