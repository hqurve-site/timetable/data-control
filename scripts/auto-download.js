// @ts-check

// This script downloads the listing and resources to the desired directory.
// It also parses the listing

const {XMLParser} = require("fast-xml-parser")
const assert = require('assert');
const fs = require('fs');

const datefns = require('date-fns')
const {tz} = require('@date-fns/tz')

const BASE_SITE = "https://mysta.uwi.edu/timetable"
const LISTING_ADDRESS = "finder.xml"
const NUM_TRIES = 3;
const MAX_PARALLEL_DOWNLOADS = 32

const CONFIG = {
    listing_timestamp_formats: [
        // See https://date-fns.org/v4.1.0/docs/parse
        {   
            // 12/31/2024 01:59:59 PM
            regex: /\d+\/\d+\/\d+\s+\d+:\d+:\d+\s+(AM|PM)/,
            handler: g => datefns.getUnixTime(datefns.parse(g[0], "M/d/yyyy h:m:s a", new Date(), {in: tz("America/Port_of_Spain")}))
        },
        {   
            // Published 15-Sep-22 2:17:02 PM - University of the West Indies
            regex: /\d+-[A-Za-z]+-\d{2}\s+\d+:\d+:\d+\s+(AM|PM)/,
            handler: g => datefns.getUnixTime(datefns.parse(g[0], "d-MMM-yy h:m:s a", new Date(), {in: tz("America/Port_of_Spain")}))
        },
    ],
    link_patterns: id => [
        `s${id}.pdf`,
        `m${id}.pdf`,
        `r${id}.pdf`,
    ]
}


main(process.argv.slice(2))
.catch((error) => {
    throw error
});

/**
 * @param {Array.<string>} args 
 */
async function main(args) {
    if (args.length === 1) {
        run(args[0])
    }else{
        throw `invalid args ${args}`;
    }
}

/**
 * @param {string} download_path 
 */
async function run(download_path) {
    // clear out the directory
    console.log(`clearing "${download_path}"`)
    for (let filename of fs.readdirSync(download_path)) {
        if (!['.git', '.gitignore'].includes(filename)) {
            // console.log(filename)
            fs.rmSync(`${download_path}/${filename}`, {recursive: true})
        }
    }

    // download xml file
    console.log("Downloading xml")
    await download(`${BASE_SITE}/${LISTING_ADDRESS}`, `${download_path}/listing.xml`);
    console.log("Finished downloading")

    // find all resources
    let listing = parse_listing_xml(`${download_path}/listing.xml`, `${download_path}/listing.json`);

    // Download in parallel using a worker based system
    console.log("Downloading pdfs")
    fs.mkdirSync(`${download_path}/resources`);
    let successful = [];
    let failed = [];
    await run_parallel_max(MAX_PARALLEL_DOWNLOADS, listing.resources, async (resource) => {
        let id = resource.id;
        let links = [resource.link];
        let errors = []
        for (let link of CONFIG.link_patterns(id)) {
            if (!links.includes(link)) links.push(link)
        }
        // try to download
        for (let link of links) {
            try {
                await download(`${BASE_SITE}/${link}`, `${download_path}/resources/${id}.pdf`)
                errors = [];
                break;
            }catch(e) {
                errors.push(...e);
            }
        }
        if (errors.length) {
            let error_string = JSON.stringify(errors);
            failed.push([id, error_string]);
            console.error([id, error_string])
        }else{
            successful.push(id)
        }
    });
    console.log("Finished downloading pdfs")

    // Save diagnostics
    console.log("Saving lists")
    successful.sort();
    failed.sort();
    fs.writeFileSync(`${download_path}/successful_downloads.json`, JSON.stringify(successful));
    if (failed.length) {
        fs.writeFileSync(`${download_path}/failed_downloads.json`, JSON.stringify(failed));
    }
    console.log("Done")
}

/**
 * @param {string} url 
 * @param {string} save_path
 */
async function download(url, save_path) {
    let errors = [];
    while (errors.length < NUM_TRIES) {
        if (errors.length > 0) {
            console.debug(`Retrying(${errors.length}) "${url}"`);
        }
        try {
            await _download();
            return;
        }catch (e) {
            errors.push(e)
        }
    }
    throw errors.map(e=> [url, e]);

    /**
     * @returns {Promise}
     */
    async function _download() {
        let response = await fetch(url);

        if (!response.ok) {
            throw `Invalid status code: ${response.status}`;
        }
        let bytes = await response.arrayBuffer();

        fs.writeFileSync(save_path, Buffer.from(bytes));
    }
}

/**
 * @param {string} xml_path 
 * @param {string} save_path
 * @returns {{timestamp: number, resources:Array.<[{id: string, link: string}]>}}
 */
function parse_listing_xml(xml_path, save_path) {
    let parser = new XMLParser({
        ignoreAttributes: false,
    });
    const json = parser.parse(fs.readFileSync(xml_path));
    fs.writeFileSync(save_path.replace(".json", ".raw.json"), JSON.stringify(json))

    let output = {};

    // find timestamp
    let footer_string = json.finder.option.footer["#text"];
    let timestamp = null;
    for (let {regex, handler} of CONFIG.listing_timestamp_formats) {
        let match = regex.exec(footer_string);
        if (match !== null) {
            timestamp = handler(match)
        }
    }
    assert(timestamp !== null, "Failed to parse timestamp")
    output.timestamp = timestamp
    
    output.resources = [];
    for (let resource of json.finder.resource) {
        let v = (key) => {
            let value = resource[key];
            assert(value != undefined, `Missing ${key} in ${JSON.stringify(resource)}`)
            return value;
        }
        // console.log(r);
        output.resources.push({
            id: v("@_id"),
            name: v("name"),
            type: [v("@_typeid"), v("@_type")],
            department: [+v("@_dept"), v("dept")],
            faculty: [+v("@_faculty"), v("faculty")],
            link: v("@_link")
        })
    }

    fs.writeFileSync(save_path, JSON.stringify(output, null, 2));
    return output
}

/**
 * @param {number} max_parallel 
 * @param {Array} list
 * @param {(any) => Promise} handler
 * @returns {Promise}
 */
function run_parallel_max(max_parallel, list, handler) {
    let queue = [...list];
    let workers = Array(max_parallel).fill(null).map(() => new Promise(async (resolve, reject) => {
        let value;
        // while there are things to do
        while ((value = queue.pop())) {
            if (queue.length % 100 == 0) {
                console.log(`Remaining: ${queue.length} (${queue.length /list.length * 100}%)`)
            }
            await handler(value);
        }
        resolve(undefined);
    }));
    return Promise.all(workers);
}