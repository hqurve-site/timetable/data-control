// @ts-check

let fs = require('fs');
let path = require('path');
let yaml = require('yaml');

if (require.main === module) {
    main(process.argv.slice(2));
}

module.exports.infer_download = infer_download;
module.exports.infer_parse = infer_parse;
module.exports.infer_unify = infer_unify;

/**
 * @param {Array.<string>} args 
 * Expected in the form: "infer" [config_type] [location]
 */
function main(args){
    if (args[0] === "infer" && args.length == 3) {
        let dir = args[2];
        if (args[1] === "download") {
            infer_download(dir)
        }else if (args[1] === "parse") {
            infer_parse(dir)
        }else if (args[1] === "unify") {
            infer_unify(dir)
        }else if (args[1] === "all") {
            main(['infer', 'download', dir]);
            main(['infer', 'parse', dir]);
            main(['infer', 'unify', dir]);
        }else{
            throw `invalid config type in args "${args}"`
        }
    }else{
        throw `invalid args "${args}"`
    }
}

function read_json(file) {
    return JSON.parse(fs.readFileSync(file+".json", 'utf8'))
}
function read_yaml(file) {
    return yaml.parse(fs.readFileSync(file + ".yaml", 'utf8'))
}
function write_yaml(object, file) {
    fs.mkdirSync(path.dirname(file), {recursive: true});
    fs.writeFileSync(file+".yaml", yaml.stringify(object, {aliasDuplicateObjects: false}), 'utf8')
}

function infer_download(dir){
    if (fs.existsSync(`${dir}/download/failed_downloads`)) {
        let failed_downloads = read_json(`${dir}/download/failed_downloads`);
        if (failed_downloads.length > 0) {
            write_yaml(failed_downloads, `${dir}/inference/failed_downloads`)
        }else{
            console.log("No failed downloads")
        }
    }else{
        console.log("No failed downloads")
    }

}

/** 
 * @returns {{
 *   page_code: RegExp,
 *   page_name: RegExp,
 *   session_type: RegExp,
 *   session_skipped: RegExp
 * }}
 */
function get_known(dir){
    let parse_config = read_json(`${dir}/parse_config`).parse;

    let page_codes = parse_config.page_code_formatter
        .map(([r]) => r).join('|')
    let page_names = parse_config.page_name_formatter
        .map(([r]) => r).join('|')
    let session_types = parse_config.session_type_formatter
        .map(([r]) => r).join('|') + "|()"
    let session_skipped = [
        /Course(s)?:/,
        /(-\s*)?Room(s)?(:|-)?/,
        /;/,
        /Staff:/,
        /Venue:/,
        /Mode:/,
        /Location:\s*\(/,
        /\)\s*and/,
        /[,;()/ \.-]/, // these random single characters are allowed
        /\(\*\)(;)?/ // (*)
    ].map(r => `(\\s*${r.source}\\s*)`).join('|') + "|(\s*)"
        
    return {
        page_code: RegExp(`^(${page_codes})$`),
        page_name: RegExp(`^(${page_names})$`),
        session_type: RegExp(`^(${session_types})$`),
        session_skipped: RegExp(`^(${session_skipped})$`),
    }
}

function infer_parse(dir) {
    let failed_parses = read_json(`${dir}/parse/failed_parses`);
    if (failed_parses.length > 0) {
        write_yaml(failed_parses, `${dir}/inference/failed_parses`)
    }else{
        console.log("No failed parses")
    }

    // gather unknown
    let successful_parses = read_json(`${dir}/parse/successful_parses`)
    let known = get_known(dir);
    let unknown_by_resource = {};
    for (let rid of successful_parses) {
        let parse = read_json(`${dir}/parse/resources/${rid}`);
        let unknown = {};
        for (let page of parse) {
            // we check session_skipped, session_type, page_code, page_name
            if (!known.page_code.test(page.meta.code)) {
                unknown.page_code = unknown.page_code || new Set();
                unknown.page_code.add(page.meta.code);
            }
            if (page.meta.name!= null && !known.page_name.test(page.meta.name)) {
                unknown.page_name = unknown.page_name || new Set();
                unknown.page_name.add(page.meta.name);
            }
            for (let session of page.sessions) {
                if (!known.session_type.test(session.type)) {
                    unknown.session_type = unknown.session_type || new Set();
                    unknown.session_type.add(session.type);
                }
                for (let skipped of session.skipped) {
                    if (!known.session_skipped.test(skipped)) {
                        unknown.session_skipped = unknown.session_skipped || new Set();
                        unknown.session_skipped.add(skipped);
                    }
                }
            }
        }

        if (Object.keys(unknown).length > 0){
            unknown_by_resource[rid] = unknown;
        }
    }
    if (Object.keys(unknown_by_resource).length > 0){
        write_yaml(unknown_by_resource, `${dir}/inference/unknown_by_resource`)
    }else{
        console.log("No unknown")
    }

    // Produce what the course config file should be.
    // We only guess the names
    let extra_course_names = new Set(Object.values(unknown_by_resource).flatMap(unknown => [...unknown.page_name || []]))
    if (extra_course_names.size > 0) {
        let course_config = read_yaml('config/course');
        course_config.name.push(...extra_course_names)
        course_config.name.sort((a,b) => {
            if (Array.isArray(a)) a = a[0];
            if (Array.isArray(b)) b = b[0];

            a = a.toLowerCase();
            b = b.toLowerCase();
            if (a < b) return -1;
            if (a > b) return 1;
            return 0;
        });
        write_yaml(course_config, `${dir}/inference/course`)
    }
}


function infer_unify(dir) {
    let unify_warnings = read_json(`${dir}/timetable/unify_warnings`);
    if (unify_warnings.length > 0) {
        write_yaml(unify_warnings, `${dir}/inference/unify_warnings`)
    }else{
        console.log("No unify warnings")
    }
}