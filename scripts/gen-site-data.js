//@ts-check

/**
 * Generate the data used in the website
 * Takes a single argument which points to the output directory
 */

const assert = require('assert');
const fs = require('fs');

// [day according to session property, {offset relative to week start, display string}]
// Note that this is in the order according to Date.getDay()
const SITE_DAYS = [
    [1, {offset:  6, string: 'Sunday'}],
    [2, {offset:  0, string: 'Monday'}],
    [3, {offset:  1, string: 'Tuesday'}],
    [4, {offset:  2, string: 'Wednesday'}],
    [5, {offset:  3, string: 'Thursday'}],
    [6, {offset:  4, string: 'Friday'}],
    [7, {offset:  5, string: 'Saturday'}],
]

const semester_info = {
    '2021/22-Semester2': {
        name: '2021/22 Semester 2',
        start_date: computeStartDate(new Date('2022-04-16'), 13)
    },
    '2021/22-Semester3': {
        name: '2021/22 Semester 3',
        start_date: computeStartDate(new Date('2022-06-07'), 40)
    },

    '2022/23-Semester1': {
        name: '2022/23 Semester 1',
        start_date: computeStartDate(new Date('2022-09-05'), 1)
    },
    '2022/23-Semester2': {
        name: '2022/23 Semester 2',
        start_date: computeStartDate(new Date('2023-01-09'), 1)
    },
    '2022/23-Semester3': {
        name: '2022/23 Semester 3',
        start_date: computeStartDate(new Date('2023-05-22'), 1)
    },

    '2023/24-Semester1': {
        name: '2023/24 Semester 1',
        start_date: computeStartDate(new Date('2023-09-04'), 1)
    },
    '2023/24-Semester2': {
        name: '2023/24 Semester 2',
        start_date: computeStartDate(new Date('2024-01-22'), 1)
    },
    '2023/24-Semester3': {
        name: '2023/24 Semester 3',
        start_date: computeStartDate(new Date('2024-05-22'), 1)
    },

    '2024/25-Semester1': {
        name: '2024/25 Semester 1',
        start_date: computeStartDate(new Date('2024-09-02'), 1)
    },
    '2024/25-Semester2': {
        name: '2024/25 Semester 2',
        start_date: computeStartDate(new Date('2025-01-20'), 1)
    },
}

main(process.argv.slice(2))

/**
 * @param {Array.<string>} args 
 */
function main(args) {
    assert(args.length === 1);
    let output_path = args[0];

    let catalog = JSON.parse(fs.readFileSync('data/catalog.json', 'utf-8'));

    if (fs.existsSync(`${output_path}/timetables`)) {
        fs.rmSync(`${output_path}/timetables`, {recursive: true});
        fs.rmSync(`${output_path}/info.json`);
    }
    fs.mkdirSync(`${output_path}/timetables`, {recursive: true})

    let config = {
        days: SITE_DAYS,
        profiles: catalog.map(entry => gen_single(output_path, entry))
    };

    fs.writeFileSync(`${output_path}/info.json`, JSON.stringify(config));
}

// Produces the profile for the site and build and copies the timetable data
function gen_single(out_path, entry) {
    let {commit_hash, date, semester} = entry;

    let profile = {
        id: commit_hash,
        type: 'static',

        publish_timestamp: new Date(date).getTime(),
        semester: semester_info[semester].name,

        start_date: semester_info[semester].start_date,
        container_types: ['course', 'room'],

        timetable_days: [2,3,4,5,6,7,1], // sunday last
        timetable_hours: new Array(22-8).fill(0).map((_z, i) => i + 8),
    };

    // get timetable file
    let initial_timetable = JSON.parse(fs.readFileSync(`data/timetables/${commit_hash}.json`, 'utf-8'));
    let timetable = conform_timetable(initial_timetable);
    fs.writeFileSync(`${out_path}/timetables/${commit_hash}.json`, JSON.stringify(timetable))


    return profile;
}

// Converts output of extractor to the form which the site uses
function conform_timetable(input_timetable) {
    let sessions = input_timetable.sessions.filter(s => s.enabled);
    let containers = input_timetable.containers.filter(c => c.enabled);

    // store the set of known ids of sessions and containers
    let session_ids = new Set(sessions.map(s=> s.id))
    let container_ids = new Set(containers.map(c=> c.id))

    // Do the following transformations
    // 1. Ensure last_modified_timestamp is null or valid
    // 2. parse times from strings (hh:mm:ss) to number of seconds since midnight
    // 3. For each session, change containers to containerIds and ensure they exist
    // 4. Do the same as above for each container and sessionIds
    for (let session of sessions) {
        session.last_modified_timestamp = session.last_modified_timestamp ?? null;
        function parse_time(time) {
            let regex = /^(\d{2}):(\d{2}):(\d{2})$/;
            let [,h,m,s] = regex.exec(time);
            return (+h * 3600) + (+m * 60) + (+s);
        }
        session.timeperiod.start = parse_time(session.timeperiod.start);
        session.timeperiod.end = parse_time(session.timeperiod.end);

        session.containerIds = session.containers.filter(id => container_ids.has(id));
        delete session.containers;
    }
    for (let container of containers) {
        container.last_modified_timestamp = container.last_modified_timestamp ?? null
        container.sessionIds = container.sessions.filter(id => session_ids.has(id));
        delete container.sessions;
    }

    // return
    let timetable = {
        sessions,
        containers
    };

    return timetable
}

/**
 * Takes a date and what week it is on and computes the start date
 * @param {Date} date 
 */
function computeStartDate(date, week) {
    let day_offset = SITE_DAYS[date.getUTCDay()][1].offset;
   
    // note that set date fixes the date to be correct
    date.setUTCDate(date.getUTCDate() - week * 7 - day_offset);
  
    return {
        year: date.getUTCFullYear(),
        month: date.getUTCMonth() + 1,
        day: date.getUTCDate(),
    };
}