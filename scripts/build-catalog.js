// @ts-check

/**
 * This file takes the path of the data-history repo, and either
 * - collects all the information (date, semester, hash) of each commit.
 * - takes each commit and extracts the contents of the pdfs
 */

let d = d => new Date(d)
let time_ranges = {
    "2021/22-Semester2": [d("2022-01"), d("2022-04")],
    "2021/22-Semester3": [d("2022-05"), d("2022-07")],

    "2022/23-Semester1": [d("2022-08"), d("2022-11-05")],
    "2022/23-Semester2": [d("2023-01"), d("2023-04")],
    "2022/23-Semester3": [d("2023-05"), d("2023-07")],

    "2023/24-Semester1": [d("2023-07-28"), d("2023-10-28")],
    "2023/24-Semester2": [d("2023-12-22"), d("2024-04-06")],
    "2023/24-Semester3": [d("2024-05"), d("2024-06-04")],

    "2024/25-Semester1": [d("2024-08"), d("2024-11-06")],
    "2024/25-Semester2": [d("2024-12-13"), Date.now()],
}


const assert = require('assert');
const fs = require('fs');
const child_process = require('child_process');
const simpleGit = require('simple-git');

main(process.argv.slice(2))
.catch((error) => {
    throw error
});

/**
 * @param {Array.<string>} args 
 */
async function main(args) {
    if (args.length === 2 && args[0] === "gather-commits") {
        await gather_commits(args[1])
    }else if (args.length === 3 && args[0] === "parse-single") {
        await parse_commit(args[1], args[2])
    }else if (args.length === 2 && args[0] === "parse-missing") {
        await parse_missing(args[1])
    }else if (args.length === 2 && args[0] === "parse-unknown") {
        await parse_unknown(args[1])
    }else{
        throw `Invalid args "${args}"`
    }


}

/**
 * Gets the 
 * @param {string} repo_path 
 */
async function gather_commits(repo_path) {
    /** @type {simpleGit.SimpleGit} */
    let repo = simpleGit(repo_path);

    let log = await repo.log(['--all']);
    
    let catalog = [];
    for (let commit of log.all) {
        // the only messages should be 
        if (commit.message === "init") continue;
        let date = new Date(commit.message);

        // determine the semester
        let semester = null;
        for (let [label, [start, end]] of Object.entries(time_ranges)){
            if (start < date && date < end) {
                assert(semester === null)
                semester = label
            }
        }
        assert(semester !== null, `Failed to find semester for ${date}: "${commit.message}" ${commit.hash}`);

        // store in catalog
        catalog.push({
            commit_hash: commit.hash,
            date: commit.message,
            semester
        });
    }
    // sort the catalog by commit date
    // Note that due to ISO format, sorting lexicographically by string is equivalent to sorting by date
    catalog.sort((a,b) => a.date.localeCompare(b.date))

    fs.writeFileSync("data/catalog.json", JSON.stringify(catalog, null, 2));
}

/**
 * This function finds all hashes for which there is no timetable file in data/timetables
 * and runs the parse_commit function
 * 
 * @param {string} extractor_run_path 
 */
async function parse_missing(extractor_run_path) {
    /** @type {Array} */
    let missing_parses = JSON.parse(fs.readFileSync("data/catalog.json", 'utf-8'))
        .filter(entry => !fs.existsSync(`data/timetables/${entry.commit_hash}.json`))
    ;

    for (let [index, entry] of missing_parses.entries()) {
        let hash = entry.commit_hash;
        console.log()
        console.log("---------------------------")
        console.log(`Running for ${hash} (${index+1} of ${missing_parses.length})`)
        console.log(`Semester: ${entry.semester}`)
        console.log(`Date: ${entry.date}`)
        
        await parse_commit(extractor_run_path, hash);
    }
}

/**
 * This function finds all hashes for which there is no timetable file in data/timetables
 * and runs the parse_commit function
 * 
 * @param {string} extractor_run_path 
 */
async function parse_unknown(extractor_run_path) {
    /** @type {Array} */
    let missing_parses = JSON.parse(fs.readFileSync("data/catalog.json", 'utf-8'))
        .filter(entry => fs.existsSync(`data/inference/${entry.commit_hash}-unknown_by_resource.yaml`))
    ;

    for (let [index, entry] of missing_parses.entries()) {
        let hash = entry.commit_hash;
        console.log()
        console.log("---------------------------")
        console.log(`Running for ${hash} (${index+1} of ${missing_parses.length})`)
        console.log(`Semester: ${entry.semester}`)
        console.log(`Date: ${entry.date}`)
        
        await parse_commit(extractor_run_path, hash);
    }
}


/**
 * This function runs the extractor in the given path.
 * Expected layout
 * - download: contains the download history repo (possibly a symlink)
 * - extractor: binary for the extractor
 * It also copies the (important) outputs to the data directory
 * @param {string} extractor_run_path
 * @param {string} commit_hash 
 */
async function parse_commit(extractor_run_path, commit_hash) {
    /** @type {simpleGit.SimpleGit} */
    let history_repo = simpleGit(`${extractor_run_path}/download`);
    // normalize hash
    commit_hash = await history_repo.revparse([commit_hash]);

    // ensure correct layout
    assert(fs.existsSync(`${extractor_run_path}/download`))
    assert(fs.existsSync(`${extractor_run_path}/extractor`) || fs.existsSync(`${extractor_run_path}/extractor.exe`))

    // clear output files
    if (fs.existsSync(`data/timetables/${commit_hash}.json`)) {
        fs.rmSync(`data/timetables/${commit_hash}.json`);
    }
    if (fs.existsSync(`data/tracking/${commit_hash}.json`)) {
        fs.rmSync(`data/tracking/${commit_hash}.json`);
    }
    if (fs.existsSync('data/inference')) {
        for (let filename of fs.readdirSync('data/inference')) {
            if (filename.startsWith(commit_hash)) {
                fs.rmSync(`data/inference/${filename}`);
            }
        }
    }

    // clear inference folder
    if (fs.existsSync(`${extractor_run_path}/inference`)) {
        fs.rmSync(`${extractor_run_path}/inference`, {recursive: true})
    }

    // build config
    console.log("building configs")
    fs.writeFileSync(`${extractor_run_path}/parse_config.json`, JSON.stringify(require('../config/index').make_parse_config()));
    fs.writeFileSync(`${extractor_run_path}/unify_config.json`, JSON.stringify(require('../config/index').make_unify_config()));

    // checkout desired hash
    await history_repo.checkout(commit_hash);

    // run the parse-parallel command
    console.log("running extractor parse")
    let result;
    result = child_process.spawnSync('./extractor', ['parse-parallel'], {
        cwd: extractor_run_path,
        stdio: 'inherit'
    });
    if (result.error) throw result.error;
    if (result.status !== 0) throw "Non-zero exit code";

    // run the unify command
    console.log("running extractor unify")
    result = child_process.spawnSync('./extractor', ['unify-timetable'], {
        cwd: extractor_run_path,
        stdio: 'inherit'
    });
    if (result.error) throw result.error;
    if (result.status !== 0) throw "Non-zero exit code";

    // run inference
    console.log('running inference')
    require('./inference').infer_parse(extractor_run_path);
    require('./inference').infer_unify(extractor_run_path);

    // save outputs
    fs.mkdirSync(`data/timetables`,{recursive: true})
    fs.writeFileSync(
        `data/timetables/${commit_hash}.json`,
        JSON.stringify(
            JSON.parse(fs.readFileSync(`${extractor_run_path}/timetable/initial.json`, 'utf-8'))
        , null, 2)
    )
    fs.mkdirSync(`data/tracking`,{recursive: true})
    fs.writeFileSync(
        `data/tracking/${commit_hash}.json`,
        JSON.stringify(
            JSON.parse(fs.readFileSync(`${extractor_run_path}/timetable/initial_tracking_info.json`, 'utf-8'))
        , null, 2)
    )

    if (fs.existsSync(`${extractor_run_path}/inference`)) {
        fs.mkdirSync(`data/inference`,{recursive: true})
        for (let filename of fs.readdirSync(`${extractor_run_path}/inference`)) {
            fs.copyFileSync(
                `${extractor_run_path}/inference/${filename}`,
                `data/inference/${commit_hash}-${filename}`
            )
        }
    }

}