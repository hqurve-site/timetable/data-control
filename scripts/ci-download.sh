#!/bin/sh
set -ex
mkdir -p out
git clone --depth=1 --single-branch --branch=rolling git@gitlab.com:hqurve-site/timetable/data-history.git out/_ci-download
npm run auto-download out/_ci-download
npm run commit-data-history out/_ci-download
cd out/_ci-download
git push
cd ../..
rm -rf out/_ci-download