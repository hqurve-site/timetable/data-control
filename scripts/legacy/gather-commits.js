// @ts-check

// this file copies the old data into the new format.
// The format is as follows
/**
 * listing.json:
 *   timestamp: number
 *   resources: array of
 *     id: string,
 *     name: string,
 *     type: [string, string] (optional)
 *     department: [string, string] (optional)
 *     faculty: [string, string] (optional)
 *     link: string (optional)
 * listing.xml (optional)
 * resources/{id}.pdf
 * successful_downloads.json: array of [id]
 * failed_downloads.json: array of [id, reason] (missing if empty)
 */

const assert = require('assert');
const fs = require('fs');
const yaml = require('yaml');
const simpleGit = require('simple-git');

const ROOT = "out/data-transition-2024-11-23"
const OLD_REPO_PATH = `${ROOT}/data-dump`
const NEW_REPO_PATH = `${ROOT}/data-history`


const branches = {
    '2021-Semester2': {},
    '2021-Semester3': {},
    '2022-Semester1': {},
    '2022-Semester2': {},
    '2022-Semester3': {},
    '2023-Semester1': {},
    '2023-Semester2': {},
    '2023-Semester3': {},
    '2024-Semester1': {},
}
console.error("The repo has already been migrated. This code should only be used for reference");
process.exit(1)
// main(process.argv.slice(2))
// .catch((error) => {
//     throw error
// });

/**
 * @param {Array.<string>} args 
 */
async function main(args) {
    if (args[0] === "single-test" && args.length === 2) {
        run_single(args[1], null);
    }else if(args[0] === "single-test" && args.length === 3) {
        run_single(args[1], args[2]);
    }else if(args[0] === "all" && args.length === 1) {
        for (let [branch,] of Object.entries(branches)) {
            console.log("======================")
            console.log(branch)
            console.log("======================")
            await run_single(branch, branch);
        }
    }else{
        throw `invalid args ${[args]}`
    }
}

/** 
 *  @param {string} branch
 *  @param {string?} new_branch_name
 */
async function run_single(branch, new_branch_name) {
    /** @type {simpleGit.SimpleGit} */
    let old_repo = simpleGit(OLD_REPO_PATH);

    /** @type {simpleGit.SimpleGit} */
    let new_repo = simpleGit(NEW_REPO_PATH);

    let old_commits = await old_repo.log([`origin/${branch}`]);
    
    // we will work on a detached head starting from master
    await new_repo.checkout('master', ['--detach']);

    // Ensure the new repo is empty (all non-hidden files)
    let initial_files = fs.readdirSync(NEW_REPO_PATH)
        .filter(name => !['.git', '.gitignore'].includes(name))
    ;
    if (initial_files.length !== 0) {
        throw `New repo not empty: ${initial_files}`
    }

    // For each commit, 
    //  1) delete all files from the new repo
    //  2) Determine all the files in the old repo
    //  3) Copy over the files
    //  4) Check if a commit has to be made

    // skip commits until we have one that has a data_version
    let has_data = false;
    for (let commit of old_commits.all.slice().reverse()) {
        await old_repo.checkout(commit.hash);

        // skip commit if we are still in the start and the file does not exist
        if (!has_data && !fs.existsSync(`${OLD_REPO_PATH}/data_version`)) {
            continue;
        }

        let version = fs.readFileSync(`${OLD_REPO_PATH}/data_version`, 'utf-8').trim();

        console.log([commit.hash, commit.date, version]);

        // clear out files in new_repo
        for (let filename of fs.readdirSync(`${NEW_REPO_PATH}`)) {
            if (!['.git', '.gitignore'].includes(filename)) {
                // console.log(filename)
                fs.rmSync(`${NEW_REPO_PATH}/${filename}`, {recursive: true})
            }
        }

        if (version === "1") {
            // try to copy if it exists.
            let listing_path = `${OLD_REPO_PATH}/data/download/listing.yaml`;
            if (fs.existsSync(listing_path)) {
                has_data = true;
                let listing = readYAML(listing_path);
                writeJSON(`${NEW_REPO_PATH}/listing.json`, listing);

                // copy each of the resources (pdfs) and note which were successful
                fs.mkdirSync(`${NEW_REPO_PATH}/resources`);
                let successful_downloads = [];
                for (let file of fs.readdirSync(`${OLD_REPO_PATH}/data/download/pdfs`)) {
                    fs.copyFileSync(
                        `${OLD_REPO_PATH}/data/download/pdfs/${file}`,
                        `${NEW_REPO_PATH}/resources/${file}`)
                    // trim off the .pdf
                    assert(file.endsWith(".pdf"))
                    successful_downloads.push(file.substring(0, file.length - 4))
                }
                
                // save the successful and failed downloads
                successful_downloads.sort();
                writeJSON(`${NEW_REPO_PATH}/successful_downloads.json`, successful_downloads)

                let failed_downloads = listing.resources.map(v => v.id)
                    .filter(id => !successful_downloads.includes(id))
                    .map(id => [id, ""]) // add no reason as to why they failed
                    .sort()
                ;
                if (failed_downloads.length !== 0) {
                    throw `${failed_downloads}`;
                }
                if (failed_downloads.length !== 0) {
                    writeJSON(`${NEW_REPO_PATH}/failed_downloads.json`, failed_downloads);
                }
                
                // perform the commit
                let timestamp = new Date(listing.timestamp * 1000).toISOString()
                await new_repo.add('.')
                await new_repo.commit(timestamp);
            }
        }else if (version === "2" || version === "2.1") {
            let listing_path = `${OLD_REPO_PATH}/data/listing(parse)/output.yaml`;
            if (fs.existsSync(listing_path)) {
                has_data = true;
                let listing = readYAML(listing_path);
                writeJSON(`${NEW_REPO_PATH}/listing.json`, listing);
                
                // copy unparsed listing if it exists
                let unparsed_listing_path = `${OLD_REPO_PATH}/data/listing(download)/output.txt`;
                if (fs.existsSync(unparsed_listing_path)) {
                    fs.copyFileSync(unparsed_listing_path, `${NEW_REPO_PATH}/listing.xml`)
                }

                // copy each of the resources (pdfs) and note which were successful
                // This folder is a mess, so we need to keep track of what files we expect and what was unexpected
                let keys_file = version === "2" ? "keys.yaml.yaml" : "keys.yaml";
                let expected_files = new Set([keys_file]);
                // use the listing file since we excluded the staff from the key set
                for (let resource_id of listing.resources.map(v => v.id)) {
                    expected_files.add(`${resource_id}.yaml`);
                    expected_files.add(`${resource_id}.pdf`);
                    expected_files.add(`${resource_id}.err`);
                }
                
                // now actually copy
                fs.mkdirSync(`${NEW_REPO_PATH}/resources`);
                let successful_downloads = [];
                let known_errors = {};
                for (let file of fs.readdirSync(`${OLD_REPO_PATH}/data/pdf(download)`)) {
                    if (!expected_files.has(file)) {
                        throw `unexpected file: ${file}`
                    }
                    expected_files.delete(file);
                    if (file.endsWith(".pdf")) {
                        fs.copyFileSync(
                            `${OLD_REPO_PATH}/data/pdf(download)/${file}`,
                            `${NEW_REPO_PATH}/resources/${file}`)
                        // trim off the .pdf
                        successful_downloads.push(file.substring(0, file.length - 4));

                        // remove error file too
                        assert(expected_files.delete(file.substring(0, file.length - 4) + ".err"));
                    }
                    if (file.endsWith(".err")) {
                        let error_string = fs.readFileSync(`${OLD_REPO_PATH}/data/pdf(download)/${file}`, 'utf-8');
                        known_errors[file.substring(0, file.length - 4)] = error_string;

                        // remove pdf file too
                        assert(expected_files.delete(file.substring(0, file.length - 4) + ".pdf"));
                        assert(expected_files.delete(file.substring(0, file.length - 4) + ".yaml"));
                    }
                }
                if (expected_files.size !== 0) {
                    throw `missing expected files ${expected_files.size}: ${JSON.stringify(expected_files)}`;
                }

                // save the successful and failed downloads
                successful_downloads.sort();
                writeJSON(`${NEW_REPO_PATH}/successful_downloads.json`, successful_downloads)

                let failed_downloads = listing.resources.map(v => v.id)
                    .filter(id => !successful_downloads.includes(id))
                    .map(id => {
                        let error_string = known_errors[id];
                        assert(typeof(error_string) === "string")
                        delete known_errors[id];
                        return [id, error_string]
                    })
                    .sort()
                ;
                if (Object.keys(known_errors).length !== 0) {
                    throw `Unaccounted known_errors: ${known_errors}`;
                }
                // console.log(failed_downloads)
                if (failed_downloads.length !== 0) {
                    writeJSON(`${NEW_REPO_PATH}/failed_downloads.json`, failed_downloads);
                }
                 
                // perform the commit
                let timestamp = new Date(listing.timestamp * 1000).toISOString()
                await new_repo.add('.')
                await new_repo.commit(timestamp);
            }
        }else{
            throw `invalid version for ${[commit.hash, commit.date, version]}`
        }
    }
    if (new_branch_name !== null) {
        new_repo.checkout(["-b", new_branch_name])
    }
}

function readYAML(path) {
    let file = fs.readFileSync(path, 'utf8');
    return yaml.parse(file)
}
function writeJSON(path, json) {
    fs.writeFileSync(path, JSON.stringify(json))
}