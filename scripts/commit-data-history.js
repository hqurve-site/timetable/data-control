// @ts-check

// This script checks if the state of the data history folder is correct and commits it
// The format is as follows
/**
 * listing.json:
 *   timestamp: number
 *   resources: array of
 *     id: string,
 *     name: string,
 *     type: [string, string] (optional)
 *     department: [string, string] (optional)
 *     faculty: [string, string] (optional)
 *     link: string (optional)
 * listing.xml (optional)
 * resources/{id}.pdf
 * successful_downloads.json: array of [id]
 * failed_downloads.json: array of [id, reason] (missing if empty)
 */

const assert = require('assert');
const fs = require('fs');
const simpleGit = require('simple-git');

main(process.argv.slice(2))
.catch((error) => {
    throw error
});

/**
 * @param {Array.<string>} args 
 */
async function main(args) {
    assert(args.length == 1);
    let repo_path = args[0];

    let listing = JSON.parse(fs.readFileSync(`${repo_path}/listing.json`, 'utf-8'));
    let successful_downloads = new Set(JSON.parse(fs.readFileSync(`${repo_path}/successful_downloads.json`, 'utf-8')));
    let failed_downloads = []
    if (fs.existsSync(`${repo_path}/failed_downloads.json`)) {
        failed_downloads = JSON.parse(fs.readFileSync(`${repo_path}/failed_downloads.json`, 'utf-8'));
        assert(failed_downloads.length !== 0);
    }

    // ensure listing = successful + failed
    let listing_copy = new Set(listing.resources.map(v => v.id));
    for (let id of successful_downloads) {
        assert(listing_copy.delete(id), `listing does not include successful ${id}`)
    }
    console.log(failed_downloads);
    for (let [id, info] of failed_downloads) {
        assert(listing_copy.delete(id), `listing does not include failed ${id} ${info}`)
    }
    assert(listing_copy.size === 0, `there are unaccounted files in the listing ${[...listing_copy]}`);
    console.log("listing matches successful and failed")
    
    // ensure that downloaded files matches the successful downloads
    let downloaded_resources = new Set(fs.readdirSync(`${repo_path}/resources`));
    for (let id of successful_downloads) {
        // remove the name from downloaded_resources
        assert(downloaded_resources.delete(id + ".pdf"), `missing download for ${id}`);
    }
    assert(downloaded_resources.size ===0, `there are unaccounted download files ${[...downloaded_resources]}`);
    console.log("successful matches resources list");


    // actually do the commit
    /** @type {simpleGit.SimpleGit} */
    let repo = simpleGit(repo_path);

    let timestamp = new Date(listing.timestamp * 1000).toISOString()
    await repo.add('.')
    await repo.commit(timestamp);
    console.log(`Committed with message: "${timestamp}"`)

}