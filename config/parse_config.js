// @ts-check

let fs = require('fs');
let path = require('path');
let yaml = require('yaml');

/** @typedef {[string, number]} PatternWithLength */
/** @typedef {['Template' | 'Literal', string]} SingleReplacement*/
/** @typedef {['Template' | 'Literal', Array.<['ContainerCode'|'Tag', string, string]>]} MultiReplacement*/

/******************
 * Declare and load config files
 *******************/ 

/** @type {Array.<string|[string, string]>} */
let session_type_config = yaml.parse(fs.readFileSync(`${__dirname}/session_types.yaml`, 'utf8'));

/** @type {{subject: Array.<string>, code: Array.<string|[string, string]>, name: Array.<string|[string, string]>}} */
let course_config = yaml.parse(fs.readFileSync(`${__dirname}/course.yaml`, 'utf8'));

/** @type {{code: Array.<string|[string, string]>, name: Array.<string|[string, string]>}} */
let room_config = yaml.parse(fs.readFileSync(`${__dirname}/room.yaml`, 'utf8'));
/** @type {Array.<[PatternWithLength, string, SingleReplacement]>} */
let extra_container_code_templates = [
    [['ENG\\s*(\\d+)', 4], 'room', ['Template', 'ENG $1']],
];

/** @type {Object.<string, Array.<string | [string, string]>>} */
let session_tag_config = yaml.parse(fs.readFileSync(`${__dirname}/session_tags.yaml`, 'utf8'));
/** @type {Array.<[PatternWithLength, MultiReplacement]>} */
let extra_session_tag_templates =[
    // [GT]<number>
    [['[GLT]\\d+', 2], ['Template', [['Tag', 'group', '$0']]]],
    // ([GT]<number>)
    [['\\(([GLT]\\d+)\\)', 4], ['Template', [['Tag', 'group', '$1']]]],

    // (Group <number>)
    [['Group\\s*(\\d+)', 6], ['Template', [['Tag', 'group', 'Group $1']]]],

    // (Cohort <number>)
    [['Cohort\\s*(\\d+)', 6], ['Template', [['Tag', 'group', 'Cohort $1']]]],

    // (G<number> [LT]<number>)
    [['\\(G(\\d+)\\s*([LT]\\d+)\\)', 6], ['Template', [
        ['Tag', 'group', 'G$1'],
        ['Tag', 'group', '$2']
    ]]],
    // (F<number> & F<number>)
    [['\\(F(\\d+)\\s*\\&\\s*F(\\d+)\\)', 6], ['Template', [
        ['Tag', 'group', 'F$1 & F$2']
    ]]],

    // ([LT]<number> - Set <number>)
    [['\\(([LT]\\d+)\\s*-\\s*Set\\s*(\\d+)\\)', 8], ['Template', [
        ['Tag', 'group', '$1'],
        ['Tag', 'group', 'Set $2']
    ]]],

    // <number> of <number>
    [['(\\d+)\\s*of\\s*(\\d+)', 4], ['Template', [['Tag', 'group', '$1 of $2']]]],

    // wow, it actually happened
    // This is needed to force the correct tokenization.
    [[convert_pattern_to_regex('FHE DCFA G Musical Arts Studio I Indian Classical Ens.'), 54],
        ['Literal', [
            ['Tag', 'class', 'Indian Classical Ens.'],
            ['ContainerCode', 'room', 'FHE DCFA G Musical Arts Studio I']
        ]]
    ],
    [[convert_pattern_to_regex('ENG 11 1st day overflow'), 19],
        ['Literal', [
            ['ContainerCode', 'room', 'ENG 11'],
            ['Tag', 'meta', '1st day overflow'],
        ]]
    ],
]


/**
 * @param {string} str 
 * @returns {string}
 */
function convert_pattern_to_regex(str) {
    // escape each of the following characters
    let meta = "\\.+*?()|[]{}^$#&-~";
    for (let m of meta.split('')) {
        // @ts-ignore
        str = str.replaceAll(m, () => `\\${m}`);
    }
    return (
        str
        .replace(/[a-zA-Z]/g, (m) => `[${m.toLowerCase()}${m.toUpperCase()}]`)
        // @ts-ignore
        .replaceAll(' ', '\\s*')
    )
}

/**********************************
 * Setup containers
 **********************************/
/**
 * @returns {{
 *      types: Array.<string>
 *      codes: Array.<[PatternWithLength, string, SingleReplacement]>,
 *      names: Array.<[PatternWithLength, SingleReplacement]>
 * }}
 */
function make_containers() {
    /** @type {{
    *      types: Array.<string>
    *      codes: Array.<[PatternWithLength, string, SingleReplacement]>,
    *      names: Array.<[PatternWithLength, SingleReplacement]>
    * }}
    */
    let output = {
        types: ['room', 'course'],
        codes: [],
        names: []
    };

    // add all codes and names
    for (let [type, file] of Object.entries({'room': room_config, 'course': course_config})) {
        for (let code of file.code) {
            let [pattern, value] = typeof code === 'string' ? [code, code] : code;
            
            output.codes.push([
                [convert_pattern_to_regex(pattern), pattern.length],
                type,
                ['Literal', value]
            ]);
        }
        for (let name of file.name) {
            let [pattern, value] = typeof name === 'string' ? [name, name] : name;
            output.names.push([
                [convert_pattern_to_regex(pattern), pattern.length],
                ['Literal', value]
            ])
        }
    }

    // add all extra stuff stuff
    for (let subject of course_config.subject) {
        let length = subject.length + 4
        let subject_regex = subject; // we shouldnt have to do anything to it
        output.codes.push([
            [`${subject_regex}\\s*(\\d{4})`, length],
            'course',
            ['Template', `${subject} $1`],
        ]);
    }
    output.codes.push(...extra_container_code_templates);

    return output
}


/**********************************
 * Setup session types
 **********************************/
/** @returns {Array.<[PatternWithLength, SingleReplacement]>} */
function make_session_types() {
    return session_type_config.map(type => {
        let [pattern, value] = typeof type === 'string' ? [type, type] : type;
        return [
            [convert_pattern_to_regex(pattern), pattern.length],
            ['Literal', value]
        ]
    })
}


/**********************************
 * Setup session tags
 **********************************/
/**
 * @returns {Array.<[PatternWithLength, MultiReplacement]>}
 */
function make_session_tags(){
    /** @type {Array.<[PatternWithLength, MultiReplacement]>} */
    let output = [];

    output.push(...extra_session_tag_templates);

    for (let [type, tags] of Object.entries(session_tag_config)) {
        for (let tag of tags) {
            let [pattern, value] = typeof tag === 'string' ? [tag, tag] : tag;

            output.push([
                [convert_pattern_to_regex(pattern), pattern.length],
                ['Literal', 
                    [['Tag', type, value]]
                ]
            ])
        }
    }

    return output;
}



module.exports.make_config = make_config;
function make_config() {
    let config = yaml.parse(fs.readFileSync(`${__dirname}/parse_config.yaml`, 'utf8'))

    // make individual stuff
    let containers = make_containers();
    let session_types = make_session_types();
    let session_tags = make_session_tags();

    // fill in header formats
    config.parse.page_header_formats = config.parse.page_header_formats.map(format =>
        format
            .replace('{code}', () => {
                /**@type {Array.<string>} */
                let regexes = containers.codes
                    .sort(([a],[b]) => -(a[1] - b[1]))// long to short
                    .map(([[regex]]) => regex);
                return regexes.join("|");
            })
            .replace('{name}', () => {
                /**@type {Array.<string>} */
                let regexes = containers.names
                    .sort(([a],[b]) => -(a[1] - b[1]))// long to short
                    .map(([[regex]]) => regex);
                return regexes.join("|");
            })
    );

    config.parse.page_type_formatter = containers.types
        .map(type => [convert_pattern_to_regex(type), 'Literal', type])
    config.parse.page_code_formatter = containers.codes
        .sort(([a],[b]) => -(a[1] - b[1]))// long to short
        .map(([[regex], _type, replacement]) => [regex, replacement[0], replacement[1]])
    config.parse.page_name_formatter = containers.names
        .sort(([a],[b]) => -(a[1] - b[1]))// long to short
        .map(([[regex], replacement]) => [regex, replacement[0], replacement[1]])

    // fill session types
    config.parse.session_formats = config.parse.session_formats.map(format => 
        format
            .replace('{type}', () => 
                session_types
                    .sort(([a],[b]) => -(a[1] - b[1]))// long to short
                    .map(([[regex]]) => regex)
                    .join('|')
            )
    )
    config.parse.session_type_formatter = session_types
        .sort(([a],[b]) => -(a[1] - b[1]))// long to short
        .map(([[regex], replacement]) => [regex, replacement[0], replacement[1]])
    
    // fill tags
    session_tags = session_tags.concat(
        containers.codes.map(([pattern, type, replacement]) => 
            [pattern, [replacement[0], [['ContainerCode', type, replacement[1]]]]]
        )
    )
    config.parse.session_tag_format = session_tags
        .sort(([a],[b]) => -(a[1]-b[1])) // long to short
        .map(([[pattern], v]) => [pattern, v[0], v[1]])

    return config
}