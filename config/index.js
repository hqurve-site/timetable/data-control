// @ts-check

let fs = require('fs');
let yaml = require('yaml');




module.exports.make_download_config = function(){
    return yaml.parse(fs.readFileSync(`${__dirname}/download_config.yaml`, 'utf8'))
}
module.exports.make_parse_config = require('./parse_config').make_config;
module.exports.make_unify_config = function(){
    return yaml.parse(fs.readFileSync(`${__dirname}/unify_config.yaml`, 'utf8'))
}

if (require.main === module) {
    main(process.argv.slice(2));
}

/**
 * @param {Array.<string>} args 
 * Expected in the form: "gen" [config_type] [location]
 */
function main(args){
    if (args[0] === "gen" && args.length == 3) {
        let output_path = args[2];
        let name;
        let config;
        if (args[1] === "download") {
            name = "download_config"
            config = module.exports.make_download_config()
        }else if (args[1] === "parse") {
            name = "parse_config"
            config = module.exports.make_parse_config()
        }else if (args[1] === "unify") {
            name = "unify_config"
            config = module.exports.make_unify_config()
        }else if (args[1] === "all") {
            main(['gen', 'download', output_path]);
            main(['gen', 'parse', output_path]);
            main(['gen', 'unify', output_path]);
            return;
        }else{
            throw `invalid config type in args "${args}"`
        }
        fs.writeFileSync(`${output_path}/${name}.json`, JSON.stringify(config));
    }else{
        throw `invalid args "${args}"`
    }

}